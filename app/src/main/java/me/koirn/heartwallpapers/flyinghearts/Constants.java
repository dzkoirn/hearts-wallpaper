package me.koirn.heartwallpapers.flyinghearts;

import me.koirn.heartwallpapers.R;

public class Constants {

    public static final int[] HEARTS_RESOURCES = {
            R.drawable.heart_1, R.drawable.heart_2, R.drawable.heart_3, R.drawable.heart_4, R.drawable.heart_5,
            R.drawable.heart_6, R.drawable.heart_7, R.drawable.heart_8, R.drawable.heart_9, R.drawable.heart_10,
            R.drawable.heart_11, R.drawable.heart_12, R.drawable.heart_13, R.drawable.heart_14, R.drawable.heart_15,
            R.drawable.heart_16, R.drawable.heart_17, R.drawable.heart_18, R.drawable.heart_19, R.drawable.heart_20,
            R.drawable.heart_21, R.drawable.heart_22, R.drawable.heart_23, R.drawable.heart_burn, R.drawable.heart_wings
    };

    public static final int[] ILU_RESOURCES = {
            R.drawable.ilu_1, R.drawable.ilu_2, R.drawable.ilu_3, R.drawable.ilu_4
    };

    public static final String SWANS_BACKGROUND = "swans_background.png";
}
