package me.koirn.heartwallpapers.flyinghearts;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import me.koirn.heartwallpapers.utils.RandomUtils;

/**
 * Created by dmitry on 28.11.15.
 */
public class FlyHeartsScene {

    private static final String LOGTAG = FlyHeartsScene.class.getSimpleName();

    static class HeartObject {
        int mHeartType;
        int x, y;
        int dx, dy;
        int rotation;
    }

    private final Random rnd = new Random(System.currentTimeMillis());

    private List<HeartObject> mHeartsObjects;

    private FlyHeartsConfig mConfig;

    private int mCanvasWidth;
    private int mCanvasHeight;


    private FlyHeartsScene(FlyHeartsConfig config, int height, int width) {
        mConfig = config;

        mCanvasWidth = width;
        mCanvasHeight = height;

        mHeartsObjects = new ArrayList<>(mConfig.getHeartsCount());

        for(int i = 0; i < mConfig.getHeartsCount(); i++) {
            HeartObject obj = new HeartObject();
            locateObject(obj);
            mHeartsObjects.add(obj);
        }
    }

    public static FlyHeartsScene create(FlyHeartsConfig config, int height, int width) {
        return new FlyHeartsScene(config, height, width);
    }

    private void locateObject(HeartObject obj) {
        obj.mHeartType = rnd.nextInt(Constants.HEARTS_RESOURCES.length);
        obj.x = rnd.nextInt(mCanvasWidth);
        obj.y = rnd.nextInt(mCanvasWidth);
        obj.rotation = RandomUtils.getIntFromInterval(rnd, -90, 90);
        switch (rnd.nextInt(4)) {
            case 1:
                obj.y = -mConfig.getHeartImageSize().first;
                obj.dx = RandomUtils.getIntFromInterval(rnd, -10, 10);
                obj.dy = RandomUtils.getIntFromInterval(rnd, 0, 10);
                break;
            case 2:
                obj.x = mCanvasWidth + mConfig.getHeartImageSize().second;
                obj.dx = RandomUtils.getIntFromInterval(rnd, -10, 0);
                obj.dy = RandomUtils.getIntFromInterval(rnd, -10, 10);
                break;
            case 3:
                obj.y = mCanvasHeight + mConfig.getHeartImageSize().first;
                obj.dx = RandomUtils.getIntFromInterval(rnd, -10, 10);
                obj.dy = RandomUtils.getIntFromInterval(rnd, -10, 0);
                break;
            case 0:
                obj.x = -mConfig.getHeartImageSize().second;
                obj.dx = RandomUtils.getIntFromInterval(rnd, 0, 10);
                obj.dy = RandomUtils.getIntFromInterval(rnd, -10, 10);
                break;
        }
    }

    void nextFrame() {
//        Log.d(LOGTAG, "nextFrame");
        for (HeartObject obj : mHeartsObjects) {
            obj.x = obj.x + obj.dx;
            obj.y = obj.y + obj.dy;
        }
    }

    void checkPosition(final List<Bitmap> heartsBitmaps) {
//        Log.d(LOGTAG, "checkPosition");
        Stream.of(mHeartsObjects)
                .forEach(heart ->
                        Stream.of(heartsBitmaps).forEach(bmp -> {
                            if (heart.x < -bmp.getWidth() || heart.x > mCanvasWidth ||
                                    heart.y < -bmp.getHeight() || heart.y > mCanvasHeight) {
                                locateObject(heart);
                            }
                        }));
    }

    void drawHearts(Canvas canvas, List<Bitmap> heartsBitmaps, Paint painter) {
//        Log.d(LOGTAG, "drawHearts");
        Stream.of(mHeartsObjects)
                .forEach(heartObj ->
                        canvas.drawBitmap(heartsBitmaps.get(heartObj.mHeartType), heartObj.x, heartObj.y, painter));
    }
}
