package me.koirn.heartwallpapers.flyinghearts;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.SurfaceHolder;

import org.apache.commons.lang3.NotImplementedException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.List;

import me.koirn.heartwallpapers.utils.BitmapUtils;

public class FlyingHeartsWallpaperService extends WallpaperService {

    private static final String LOGTAG = FlyingHeartsWallpaperService.class.getSimpleName();

    private final Handler mHandler = new Handler();

    public FlyingHeartsWallpaperService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public Engine onCreateEngine() {
        return new DrawingEngine(getApplicationContext());
    }

    class DrawingEngine extends Engine implements SharedPreferences.OnSharedPreferenceChangeListener {

        private List<Bitmap> mHeartsBitmaps;
        private List<Bitmap> mILoveUBitmaps;

        private FlyHeartsConfig mConfig;
        private FlyHeartsScene mScene;

        private Bitmap mBackgroundBitmap;

        boolean mVisible;

        Rect mCanvasRect;
        Rect mBackgroundVisibleRect;
        float mOffset;

        Paint mPaint;
        WeakReference<Context> mContextRef;

        DrawingEngine(Context context) {
            mContextRef = new WeakReference<>(context);

            mConfig = FlyHeartsConfig.create(context);
        }

        @Override
        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);
            //Note: surface size is not available here
            mPaint = new Paint();

            Context context = mContextRef.get();
            if (context != null) {
                mHeartsBitmaps = BitmapUtils.preloadDrawables(mContextRef.get(), Constants.HEARTS_RESOURCES);
                mILoveUBitmaps = BitmapUtils.preloadDrawables(mContextRef.get(), Constants.ILU_RESOURCES);
            }

            mConfig.addOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();

            mConfig.removeOnSharedPreferenceChangeListener(this);

            mHeartsBitmaps = null;
            mILoveUBitmaps = null;

            mPaint = null;
        }

        private Bitmap getBackgroundBitmap(int screenWidth, int screenHeight) {
            Bitmap bitmap = null;
            InputStream inputStream = mConfig.getBackground(mContextRef.get());
            if(inputStream != null) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                bitmap =  BitmapFactory.decodeStream(new BufferedInputStream(inputStream), null, options);
                int visibleWidth = screenWidth * bitmap.getHeight() / screenHeight;
                int left = (bitmap.getWidth() - visibleWidth) / 2;
                mBackgroundVisibleRect = new Rect(left, 0, left + visibleWidth, bitmap.getHeight());
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.e(LOGTAG, e.getMessage(), e);
                }
            }
            return bitmap;
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            //re-init values
            mBackgroundBitmap = getBackgroundBitmap(getSurfaceHolder().getSurfaceFrame().width(),
                    getSurfaceHolder().getSurfaceFrame().height());

            mScene = FlyHeartsScene.create(mConfig, getSurfaceHolder().getSurfaceFrame().width(),
                    getSurfaceHolder().getSurfaceFrame().height());
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            mVisible = visible;
            if (visible) {
                drawFrame();
            } else {
                mHandler.removeCallbacks(mDrawFrame);
            }
        }

        @Override
        public void onSurfaceCreated(SurfaceHolder holder) {
            super.onSurfaceCreated(holder);

            Log.d(LOGTAG, "onSurfaceCreated");
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);
            // store the center of the surface, so we can draw the cube in the right spot
            Log.d(LOGTAG, "onSurfaceChanged " + format + " " + width + " " + height);

            //re-init
            mCanvasRect = new Rect(0, 0, width, height);
            mBackgroundBitmap = getBackgroundBitmap(width, height);

            mScene = FlyHeartsScene.create(mConfig, height, width);

            drawFrame();
        }

        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);
            Log.d(LOGTAG, "onSurfaceDestroyed");

            mVisible = false;
            mHandler.removeCallbacks(mDrawFrame);

            mScene = null;
            mBackgroundBitmap.recycle();
            mBackgroundBitmap = null;

            mCanvasRect = null;
            mBackgroundVisibleRect = null;
        }

        @Override
        public void onOffsetsChanged(float xOffset, float yOffset,
                                     float xStep, float yStep, int xPixels, int yPixels) {
            Log.d(LOGTAG, String.format("onOffsetsChanged(xOffset = %1$f, yOffset = %2$f," +
                    " xStep = %3$f, yStep = %4$f, xPixels = %5$d, yPixels = %6$d",
                    xOffset, yOffset, xStep, yStep, xPixels, yPixels));
            mOffset = xOffset;
            drawFrame();
        }



        /*
         * Draw one frame of the animation. This method gets called repeatedly
         * by posting a delayed Runnable. You can do any drawing you want in
         * here. This example draws a wireframe cube.
         */
        void drawFrame() {
            final SurfaceHolder holder = getSurfaceHolder();

            Canvas c = null;
            try {
                c = holder.lockCanvas();
                if (c != null) {
                    // draw something
                    if(mBackgroundBitmap != null) {
                        drawBackground(c);
                    } else {
                        c.drawColor(Color.BLACK);
                    }
                    mScene.drawHearts(c, mHeartsBitmaps, mPaint);
                }
            } finally {
                if (c != null)
                    holder.unlockCanvasAndPost(c);
            }

            // Reschedule the next redraw
            mHandler.removeCallbacks(mDrawFrame);
            if (mVisible) {
                mHandler.postDelayed(mDrawFrame, 100);
            }
        }

        void drawBackground(Canvas canvas) {
//            Log.d(LOGTAG, "drawBackground");
//            Log.d(LOGTAG, "mOffset = " + mOffset);
            canvas.drawBitmap(mBackgroundBitmap, mBackgroundVisibleRect, mCanvasRect, mPaint);
//            canvas.drawBitmap(mBackgroundBitmap, mOffset, 0, mPaint);
        }

        private final Runnable mDrawFrame = new Runnable() {
            public void run() {
                mScene.checkPosition(mHeartsBitmaps);
                mScene.nextFrame();
                drawFrame();
            }
        };


    }
}
