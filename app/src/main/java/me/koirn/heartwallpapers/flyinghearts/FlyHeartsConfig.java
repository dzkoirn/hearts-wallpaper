package me.koirn.heartwallpapers.flyinghearts;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.util.Log;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import me.koirn.heartwallpapers.utils.PreferencesManager;

public class FlyHeartsConfig extends PreferencesManager.BasicPreferences {

    public static final String SHARE_PREFS_NAME = "flying_hearts";

    public static final String KEY_BACKGROUND = "background";
    public static final String KEY_HEART_SIZE_HEIGHT = "heart_size_height";
    public static final String KEY_HEART_SIZE_WIDTH = "heart_size_width";
    public static final String KEY_HEART_MAX_COUNT = "heart_max_count";
    public static final String KEY_HEART_MAX_SPEED = "heart_max_speed";

    public static final String DEFAULT_BACKROUND = "swans_background.png";
    public static final int DEFAULT_HEART_HEIGHT = 100;
    public static final int DEFAULT_HEART_WIDTH = 100;
    public static final int DEFAULT_HEART_COUNT = 10;
    public static final int DEFAULT_HEART_SPEED = 10;

    private volatile static FlyHeartsConfig sInstance;

    private SharedPreferences mPrefs;

    private String mBackground;
    private int mHeartHeight;
    private int mHeartWidth;
    private int mHeartsCount;
    private int mHeartsSpeed;

    private FlyHeartsConfig(Context context) {
        mPrefs = context.getSharedPreferences(SHARE_PREFS_NAME, Context.MODE_PRIVATE);

        mBackground = mPrefs.getString(KEY_BACKGROUND, null);

        mHeartHeight = mPrefs.getInt(KEY_HEART_SIZE_HEIGHT, DEFAULT_HEART_HEIGHT);
        mHeartWidth = mPrefs.getInt(KEY_HEART_SIZE_WIDTH, DEFAULT_HEART_WIDTH);
        mHeartsCount = mPrefs.getInt(KEY_HEART_MAX_COUNT, DEFAULT_HEART_COUNT);
        mHeartsSpeed = mPrefs.getInt(KEY_HEART_MAX_SPEED, DEFAULT_HEART_SPEED);
    }

    public synchronized  static final FlyHeartsConfig create(Context context) {
        if (sInstance == null) {
            sInstance = new FlyHeartsConfig(context);

        }
        return sInstance;
    }

    public InputStream getBackground(Context context) {
        InputStream inputStream = null;
        try {
            inputStream = context.getContentResolver().openInputStream(Uri.parse(mBackground));
        } catch (FileNotFoundException | NullPointerException e) {
            Log.e("FlyHeartsConfig", e.getMessage(), e);
            inputStream = getDefaultBackround(context);
        } finally {
            return inputStream;
        }
    }

    public static InputStream getDefaultBackround(@NonNull Context context) {
        try {
            return context.getAssets().open(DEFAULT_BACKROUND);
        } catch (IOException e) {
            return null;
        }
    }

    public void setBackgroundPath(String path) {
        mBackground = path;
    }

    /**
     * @return return Pair with height and width of hearts image
     */
    public Pair<Integer, Integer> getHeartImageSize() {
        return new Pair<>(mHeartHeight, mHeartWidth);
    }

    public int getHeartsCount() {
        return mHeartsCount;
    }

    public int getHeatsSpeed() {
        return mHeartsSpeed;
    }

    public void save(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(SHARE_PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        if (StringUtils.isNotEmpty(mBackground)) {
            editor.putString(KEY_BACKGROUND, mBackground);
        }
        editor.commit();
        //because we don't change other values we don't save it
    }

    public void addOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mPrefs.registerOnSharedPreferenceChangeListener(listener);
    }

    public void removeOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mPrefs.unregisterOnSharedPreferenceChangeListener(listener);
    }

}
