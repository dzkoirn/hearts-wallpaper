package me.koirn.heartwallpapers.flyinghearts;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


import me.koirn.heartwallpapers.R;
import me.koirn.heartwallpapers.utils.PreferencesManager;
import me.koirn.heartwallpapers.utils.RandomUtils;

public class FlyHeartWallpaperSettingsActivity extends Activity implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String LOGTAG = FlyHeartWallpaperSettingsActivity.class.getSimpleName();

    private static final int REQ_CODE_PICK_IMAGE = 0x1001;

    private ImageView mBackgroundPreview;
    private FlyHeartsConfig mConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fly_heart_settings_layout);

        mConfig = FlyHeartsConfig.create(this);

        mBackgroundPreview = (ImageView) findViewById(R.id.background_preview);

        Button okButton = (Button) findViewById(R.id.ok_button);
        Button cancelButton = (Button) findViewById((R.id.cancel_button));

        cancelButton.setOnClickListener(view -> finish());
        okButton.setOnClickListener(view -> {
            mConfig.save(getApplicationContext());
            finish();
        });

        mBackgroundPreview.setOnClickListener(view -> {
            try {
                chooseImage();
            } catch (IOException ex) {
                Log.e(LOGTAG, ex.getMessage(), ex);
                Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        InputStream inputStream = mConfig.getBackground(getApplicationContext());
        if (inputStream != null) {
            Bitmap yourSelectedImage = BitmapFactory.decodeStream(new BufferedInputStream(inputStream));
            mBackgroundPreview.setImageBitmap(yourSelectedImage);
            try {
                inputStream.close();
            } catch (IOException e) {
                Log.e(LOGTAG, e.getMessage(), e);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case REQ_CODE_PICK_IMAGE:
                if(resultCode == RESULT_OK) {
                    Uri uri = imageReturnedIntent.getData();
                    mConfig.setBackgroundPath(uri.toString());
                    InputStream imageStream = null;
                    try {
                        imageStream = new BufferedInputStream(getContentResolver().openInputStream(uri));
                        Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                        if(yourSelectedImage != null) {
                            Log.d(LOGTAG, "mBackgroundPreview setImageBitmap");
                            mBackgroundPreview.setImageBitmap(yourSelectedImage);
                        } else {
                            Log.d(LOGTAG, "yourSelectedImage is null");
                        }
                    } catch (FileNotFoundException | NullPointerException e) {
                        Log.e(LOGTAG, e.getMessage(), e);
                        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    } finally {
                        if (imageStream != null) {
                            try {
                                imageStream.close();
                            } catch (IOException e) {
                                Log.e(LOGTAG, e.getMessage(), e);
                            }
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {

    }

    private void chooseImage() throws IOException {
        Log.d(LOGTAG, "chooseInage");
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, REQ_CODE_PICK_IMAGE);
    }
}
