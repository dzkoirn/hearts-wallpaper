package me.koirn.heartwallpapers.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Pair;

public class PreferencesManager {

    public static abstract class BasicPreferences {


    }

    private SharedPreferences prefs;

    public PreferencesManager(Context context, String prefsName) {
        prefs = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);
    }

    public static final PreferencesManager create(Context context, String name) {
        return new PreferencesManager(context, name);
    }

    /**
     *
     * @param imageWidth
     * @param imageHeight
     */
    public void saveImageSize(int imageWidth, int imageHeight) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(IMAGE_WIDTH, imageWidth)
              .putInt(IMAGE_HEIGHT, imageHeight);
        editor.commit();
    }

    public Pair<Integer, Integer> getImageSize(int defaultImageWidth, int defaultImageHeight) {
        return Pair.create(prefs.getInt(IMAGE_WIDTH, defaultImageWidth),
                prefs.getInt(IMAGE_HEIGHT, defaultImageHeight));
    }

    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        prefs.registerOnSharedPreferenceChangeListener(listener);
    }

    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        prefs.unregisterOnSharedPreferenceChangeListener(listener);
    }


    /**
     *
     * @param backgroundImagePath
     */
    public void saveBackgroundImageUri(String backgroundImagePath) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(IMAGE_BACKGROUND_PATH, backgroundImagePath);
        editor.commit();
    }

    /**
     *
     * @return
     */
    public String getBackgroundImagePath() {
       return prefs.getString(IMAGE_BACKGROUND_PATH, null);
    }

    /**
     * Keys constants
     */
    private static final String IMAGE_WIDTH = "width";
    private static final String IMAGE_HEIGHT = "height";
    private static final String IMAGE_BACKGROUND_PATH = "background_path";

}
