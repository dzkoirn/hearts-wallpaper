package me.koirn.heartwallpapers.utils;

import java.util.Random;

public class RandomUtils {

    public static int getIntFromInterval(Random rndGen, int min, int max) {
        return min + rndGen.nextInt(max - min);
    }

    public static String getRandomString() {
        return String.valueOf(Math.abs(new Random(System.currentTimeMillis()).nextInt()));
    }
}
