package me.koirn.heartwallpapers.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class BitmapUtils {

    public interface InputStreamWrapper {
        public InputStream getInputStream() throws IOException;
    }

    /**
     * Decode bitmap with specifed size from input stream
     *
     * @param inputStreamWrapper
     * @param imageWidth
     * @param imageHeight
     * @return decoded bitmap
     */
    public static Bitmap decodeBitmap(InputStreamWrapper inputStreamWrapper, int imageWidth, int imageHeight) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeStream(inputStreamWrapper.getInputStream(), null, options);
        options.inSampleSize = calculateInSampleSize(options, imageWidth, imageHeight);
        options.inJustDecodeBounds = false;

        Bitmap decodedImage = BitmapFactory.decodeStream(inputStreamWrapper.getInputStream(), null, options);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(decodedImage, imageWidth, imageHeight, false);
        return scaledBitmap;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static List<Bitmap> preloadDrawables(Context context, int[] resids) {
        Resources res = context.getResources();
        List<Bitmap> bitmaps = new LinkedList<>();
        for (int resId : resids) {
            bitmaps.add(BitmapFactory.decodeResource(res, resId));
        }
        return Collections.unmodifiableList(bitmaps);
    }
}
